import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-centos')


def test_repolist(host):
    cmd = host.run('yum repolist')
    assert 'rpm-ics/x86_64' in cmd.stdout
    assert 'epel-ess/x86_64' in cmd.stdout
    assert 'base-ess/x86_64' in cmd.stdout
    assert 'extras-ess/x86_64' in cmd.stdout
    assert 'zabbix-mirror' in cmd.stdout
    assert 'updates-ess/x86_64' in cmd.stdout
    assert 'epel-testing-ess/x86_64' not in cmd.stdout
    assert 'base/7/x86_64' not in cmd.stdout
    assert 'extras/7/x86_64' not in cmd.stdout
    assert 'updates/7/x86_64' not in cmd.stdout
    assert 'ics-rpm/x86_64' not in cmd.stdout


def test_centosinstall(host):
    cmd = host.run('yum install --disablerepo=rpm-ics,epel-ess -y which')
    assert cmd.rc == 0


def test_epelinstall(host):
    cmd = host.run('yum install --disablerepo=rpm-ics,base-ess,extras-ess,updates-ess -y arj')
    assert cmd.rc == 0


def test_ics_install(host):
    cmd = host.run('yum install --disablerepo=epel-ess,base-ess,extras-ess,updates-ess -y jdk1.8')
    assert cmd.rc == 0


def test_zabbix_centos_install(host):
    cmd = host.run('yum search --disablerepo=epel-ess,base-ess,extras-ess,updates-ess -y zabbix-agent')
    assert cmd.rc == 0
