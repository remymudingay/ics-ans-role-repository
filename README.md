ics-ans-role-repository
=======================

This Ansible role enables the EPEL and ESS ICS RPM repositories.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.14

Role Variables
--------------

```yaml
repository_installroot: "/"
```

The `repository_installroot` variable allows to specify an alternative installroot, e.g. /export/nfsroots/centos7/rootfs

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-repository
```

License
-------

BSD 2-clause
